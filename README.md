# james-becker-docker-ce deployment


## How to use:

1. configure ssh access between ansible host and the docker targets
2. update host file with your docker workers, masters, and the init master which will be where your cluster is initiated from
3. from the main directory container your hosts file run: `ansible-playbook -i hosts docker-site.yml`
